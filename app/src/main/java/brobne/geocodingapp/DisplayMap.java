package brobne.geocodingapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;


public class DisplayMap extends ActionBarActivity {

    private MapFragment map;
    private LatLng coordinates;
    private TextView textView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_map);

        textView = (TextView) findViewById(R.id.address);

            //get user input address
            String address = getIntent().getStringExtra("address");
            ConvertLocation location = new ConvertLocation();
            location.execute(address);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ConvertLocation extends AsyncTask<String, Void, JSONObject>
        implements OnMapReadyCallback {

        private JSONObject jsonObject;

        private ConvertLocation() { }

        @Override
        public void onMapReady(GoogleMap m) {
            m.addMarker(new MarkerOptions().position(coordinates).title("Location You Entered"));
            m.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 13));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                if (jsonObject.get("status").equals("ZERO_RESULTS")) {
                    DialogFragment noResults = new NoResultsDialogFragment();
                    noResults.show(getSupportFragmentManager(), "no results");
                } else{

                    //get latitude, longitude to use for GoogleMap
                    double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lng");

                    double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lat");

                    coordinates = new LatLng(lat, lng);
                    String formattedAddress = (String) ((JSONArray) jsonObject.get("results"))
                            .getJSONObject(0).get("formatted_address");
                    textView.append("Location: \n");
                    textView.append(formattedAddress);

                    //create map fragment
                    map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
                    map.getMapAsync(this);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("coordinates", "" + coordinates);

        }

        /* SOURCE FOR THIS
        http://stackoverflow.com/questions/15711499/get-latitude-and-longitude
        -with-geocoder-and-android-google-maps-api-v2 */
        @Override
        protected JSONObject doInBackground(String... addresses) {
            String address = convertAddress(addresses[0]);
            String uri = "http://maps.google.com/maps/api/geocode/json?address=" + address;
            HttpGet httpGet = new HttpGet(uri);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            //builds JSONObject to parse later
            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                jsonObject = new JSONObject(stringBuilder.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        //converts whitespace to + signs for url
        private String convertAddress(String address) {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < address.length(); i++) {
                if (address.charAt(i) == (' ')) {
                    s.append("+");
                } else {
                    s.append(address.charAt(i));
                }
            }
            return s.toString();
        }
    }
}
